from lta.Parameter import Parameter
from DirCommon import DirCommon
from pathlib import Path
import  sys
startDir = Path(sys.argv[1])

#startDir = ""
dirCommon = DirCommon(startDir)
filesInDir = dirCommon.getFiles()

print ("File Name , File ID, File Version")
for file in filesInDir:
    filePath = startDir.joinpath(file)
    if not filePath.exists():
          print("Path not exist")
          continue

    parameter = Parameter(filePath)

    fileId = ""
    fileVersion = ""
    paramList = parameter.getHeaderDetails(fileId, fileVersion)
    print("{}, {}, {}".format(file, paramList[0], paramList[1]))
