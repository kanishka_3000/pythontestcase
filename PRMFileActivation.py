
import re

class PRMFileActivation:
	
	m_fileName = ""
	m_operator1 = -1
	m_operator2 = -1
	def __init__(self, fileName, operator1ID, operator2ID):
		self.m_fileName = fileName
		self.m_operator1 = operator1ID
		self.m_operator2 = operator2ID
		
	def isMatch(self, line):
		return re.match(r'(.*)Activated SPID(.*)', line, re.M|re.I)
	
	def getSPID(self, line):
		wordList = line.split()
		word = wordList[4]
		mat = re.search('SPID:(.+?),File:', line)
		return mat.group(1)
		
	def getFileName(self, line):
		wordList = line.split()
		word = wordList[5]
		return word
		
	def getFileList(self, op1FileSet, op2FileSet):
		file = open(self.m_fileName, "r")
		fileSet = set()
		for line in file:
			if self.isMatch(line) :
				fileName = self.getFileName(line)
				spid = self.getSPID(line)
				fileSet.add(spid + " : " +fileName)
			
				if 	spid == "30":# *********hard coded fix**********
					
					op1FileSet.add(fileName)
				elif spid == "31":
					
					op2FileSet.add(fileName)
		file.close()
	