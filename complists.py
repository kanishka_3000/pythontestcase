from FileCommon import FileCommon

file1 = "list1.txt"
file2 = "list2.txt"

fileHandle = FileCommon(file1)

firstSet = fileHandle.getSet()

fileHandle = FileCommon(file2)

secondSet = fileHandle.getSet()

firstToSecondSet = firstSet.difference(secondSet)
secondToFirst = secondSet.difference(firstSet)

print("******* First to second**********")

for file in firstToSecondSet:
	print(file)
	
print("******* Second to first**********")

for file in secondToFirst:
	print(file)