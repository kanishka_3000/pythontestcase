import ftplib

class FTPCommon:

	m_path = ""
	m_connection = 0
	def __init__(self, path, username, password, root):
		self.m_path = path
		self.m_connection = ftplib.FTP(path, username, password)
		self.m_connection.cwd(root)
		
	def getFiles(self):
		fileList =  self.m_connection.nlst()
		fileSet = set()
		for file in fileList:
			fileSet.add(file)
			
		return fileSet
		
	def close(self):
		self.m_connection.close()
	