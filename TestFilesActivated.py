import sys
from DirCommon import DirCommon

from PRMFileActivation import PRMFileActivation

telsDir = "C:\\SC\\Parameters\\Outgoing\\30\\0401\\Live"
tltoDir = "C:\\SC\\Parameters\\Outgoing\\31\\0401\\Live"

operator1ID =  30
operator2ID = 31
logFileName = sys.argv[1]
print("Log file name : " + logFileName)
fileActivation = PRMFileActivation(logFileName, operator1ID, operator2ID)

telsActivated = set()
tltoActivated = set()
fileActivation.getFileList(telsActivated, tltoActivated)

print("Following files were activated for operator 1")

for file in telsActivated:
	print(file)
	
print("Following files were activated for operator 2")

for file in tltoActivated:
	print(file)

print("\n**********************************************\n")

dirCommon = DirCommon(telsDir)	

telsDirFileSet =  dirCommon.getFiles()

dirCommon = DirCommon(tltoDir)

tltoDirFileSet = dirCommon.getFiles()

if len(telsDirFileSet) > len(telsActivated) or len(tltoDirFileSet) > len(tltoActivated):
	print("FAILED ALL files were not activated" )
	
print("Following files are in tels dir but not activated")

notActivatedTels = telsDirFileSet.difference(telsActivated)

for file in notActivatedTels:
	print(file)
	
notActivatedTlto = tltoDirFileSet.difference(tltoActivated)

print("Following files are in tels dir but not activated")

for file in notActivatedTlto:
	print(file)



