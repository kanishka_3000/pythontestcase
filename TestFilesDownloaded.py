from FTPCommon import FTPCommon
from DirCommon import DirCommon

telsFTPpath = "/d4/tels/data/roee/outgoing/sys"
tltoFTPpath = "/d4/tlto/data/roee/outgoing/sys"

telsDir = "C:\\SC\\Parameters\\Outgoing\\30\\0401\\Live"
tltoDir = "C:\\SC\\Parameters\\Outgoing\\31\\0401\\Live"
print("**** TestFilesDownloaded******")

ftpCommon = FTPCommon("192.168.62.61", "adapter", "adapter", telsFTPpath)

telsFTPFileSet = ftpCommon.getFiles()
ftpCommon.close()

ftpCommon = FTPCommon("192.168.62.61", "adapter", "adapter", tltoFTPpath)

tltoFTPFileSet = ftpCommon.getFiles()
ftpCommon.close()

dirCommon = DirCommon(telsDir)	

telsDirFileSet =  dirCommon.getFiles()

dirCommon = DirCommon(tltoDir)

tltoDirFileSet = dirCommon.getFiles()

telsDiffSet = telsFTPFileSet.difference(telsDirFileSet)
tltoDiffSet = tltoFTPFileSet.difference(tltoDirFileSet)
print("\n")
if len(telsDiffSet) > 0 or len(tltoDiffSet) > 0:
	print("TEST FAILED File count difference ftp vs dir")
	
print("\n")

print("**Following files are in FTP but not in dir for TELS**")

for file in telsDiffSet:
	print(file)

print("**Following files are in FTP but not in dir for TLTO**")

for file in tltoDiffSet:
	print(file)