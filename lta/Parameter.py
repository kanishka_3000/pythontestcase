from pathlib import Path
class Parameter:
    m_filePath = Path()
    def __init__(self, filePath):
        self.m_filePath = filePath

    def getHeaderDetails(self, fileIdOutput, versionOutput):
       # print("file path is" + self.m_fileName)
        try:
            paramFile = open(self.m_filePath, 'rb')
        except FileNotFoundError:
            print("File Not Found " + self.m_fileName)

        fileID = paramFile.read(2)
        formatVersion = paramFile.read(1)
        paramVersion = paramFile.read(2)

        fileIdOutput = fileID
        i = int.from_bytes(paramVersion, byteorder='big')
        versionOutput = str(i)

        retrnItems = list()
        retrnItems.append(fileID)
        retrnItems.append(versionOutput)

        return retrnItems





